### A Git Workflow Model for Continuous Delivery

**Continuous Delivery Flow (CDF)**, also known as **Semantic Versioning Flow (SVF)**, is a trunk-based development model and an extension and evolution of **[GitHub Flow](https://docs.github.com/en/get-started/using-github/github-flow)** providing a practical and pragmatic **[Git](https://git-scm.com/)** workflow that supports teams and projects where software deployments are made regularly using a **Continuous Delivery** process.

This guide explains how and why **Continuous Delivery Flow** (or **Semantic Versioning Flow**) works for **DevOps** or **DevSecOps** pipelines. If you are looking for an alternative to **[Git Flow](https://nvie.com/posts/a-successful-git-branching-model/)** and something with a little more process defined than what **[GitHub Flow](https://docs.github.com/en/get-started/using-github/github-flow)** has to offer, especially around how software versioning and branching work together, then this might be what you are looking for.

**CDF** does not stop at just branching and versioning software. It is designed to be integrated into a full **DevSecOps** solution. This allows for full integration of security scanning and test automation of software, improving the efficiency, quality, and risk associated with a **Software Development Life Cycle (SDLC)**.

## Terminology

### Continuous Integration

**Continuous Integration (CI)** is the process of frequently integrating work, from multiple developers, into an _integration_ branch, which is typically a _release (candidate)_ branch. **Continuous Integration** is typically the default starting point for any software build process (pipeline).

> <img src="./images/continuous-integration.svg" width="550" alt="Continuous Integration" title="Continuous Integration"/>

What comes next, after **Continuous Integration**, is an extension and evolution that eventually becomes **Continuous Delivery**, and with continued evolution results in **Continuous Deployment**.

### Continuous Delivery

**Continuous Delivery (CD)** builds upon **Continuous Integration (CI)** by adding a deployment component to the process. This is the process of moving software, that has been built, tested, deployed, and verified in various lower (non-production) environments, into _release candidates_ that may or may not end up in a production environment. A strong versioning strategy is required for **Continuous Delivery**, the most popular being **[Semantic Versioning](https://semver.org/)**. A release candidate is designated to be either a "good release" or a "bad release", and only "good releases" are _considered_ for deployment to production.

> <img src="./images/continuous-delivery.svg" width="770" alt="Continuous Delivery" title="Continuous Delivery"/>

### Continuous Deployment

**Continuous Deployment** is the process of promoting software straight into production, using a _fully automated_ build process consisting of a test suite that provides confidence and proves that the build is "production-ready". There are no release candidates in **Continuous Deployment**. As soon as a change is committed to the software's main integration branch, the build is either successful or it is not -- if it is successful then it gets deployed _immediately_ to production. A strong versioning strategy is not required here because the most recent source code in the main integration branch is what is in production, so any _arbitrary_ identifier can be used as a "version" (example: commit hash, build number, etc.).

> <img src="./images/continuous-deployment.svg" width="496" alt="Continuous Deployment" title="Continuous Deployment"/>

> <kbd>NOTE:</kbd> **Continuous Deployment** is rarely achieved and should only be considered once a solid **Continuous Delivery** pipeline is in place.

## Versioning

### Specification

In a **Continuous Delivery Flow**, the most common versioning specification to use is: **[Semantic Versioning](https://semver.org/)**

### Snapshots

A snapshot version (`X.Y.Z-SNAPSHOT`) represents the _goal_ you are trying to get to. For example, the snapshot version `2.3.1-SNAPSHOT` indicates that the goal is to get to the release version `2.3.1`. Consequently, the act of releasing `2.3.1-SNAPSHOT` means that you have "achieved your goal" and therefore the release process will produce the release artifact versioned as: `2.3.1`

Snapshot artifacts are _temporary_, _disposable_, and _mutable_ (_overwritable_) artifacts that are used to verify a piece of functionality (feature, bug-fix, etc.). Snapshot artifacts can never make it to a higher or production environment -- they are only used in lower environments to verify code. Snapshot artifacts get published to a snapshot repository.

> <kbd>NOTE:</kbd> **Branches in Git represent SNAPSHOTS**
>
> The source code at the _HEAD_ of every single **branch** in a Git repo should contain a snapshot version in the build file, in the format `X.Y.Z-SNAPSHOT` where:
> - `X`, `Y`, and `Z` are positive integers
> - the versioning follows the specification: **[Semantic Versioning](https://semver.org/)**

### Releases

In a **Continuous Delivery Flow**, a release version (`X.Y.Z`) represents an artifact that is _potentially_ ready to be deployed to production (it is a _release candidate_), it may not necessarily reach production for a variety of reasons.

Release artifacts are _permanent_, _immutable_ artifacts that can be deployed to any environment. Release artifacts get published to a release repository. A release candidate can be a failure, and in such cases, that release version number cannot be re-used, and a new release with a new release version number must be created to fix whatever issues were found in the failed release. Release artifacts should live forever in their release repository unless explicitly deleted.

> <kbd>NOTE:</kbd> **Tags in Git represent RELEASES**
>
> In order to view the source code of a release (`X.Y.Z`) that is currently running in any environment, you must refer to the Git **tag** associated to the version of the code running in that environment. Tags, _not_ branches, in Git are associated to releases because branches, regardless of their name, are associated to snapshots. See [Release Process](#Release_Process) to better understand why this is.

## Branching

### Misconceptions

There are several misconceptions about the purpose of certain branches due to the misunderstanding of whether **Continuous Delivery** vs **Continuous Deployment** is implemented.

The notion that the `main` branch (or any other release branch) represents the code in production is simply _not true_ in a **Continuous Delivery** process -- this is only true in a **Continuous Deployment** process where every (successful) code commit is pushed to production. In the **Continuous Delivery Flow**, the code deployed to production is a RELEASE and consequently is associated to a Git _tag_ (not a Git _branch_, such as `main`, or any other branch).

> <kbd>NOTE:</kbd> Branches that are called `release/X.Y.z` still represent a SNAPSHOT. Remember: the HEAD of _any_ given branch is a SNAPSHOT (the goal being: to get to a RELEASE, with that RELEASE being associated to a Git _tag_).

There are _work_ branches and there are _release candidate_ (integration, maintenance) branches. Work branches are temporary and release branches have various lifespans, with the `main` branch living forever.

### Integration Branches

These types of branches are where _release candidates_ are created from. These branches can act as release maintenance branches or for developing various releases in parallel. The version of the software must be unique within each release candidate branch (no two release candidate branches can share the same version number).

The version specified in the _HEAD_ of this type of branch should always be a SNAPSHOT version in the build file, in the format `X.Y.Z-SNAPSHOT` where:
- `X`, `Y`, and `Z` are positive integers
- the versioning follows the specification: **[Semantic Versioning](https://semver.org/)**

There are 3 types of _release candidate_ (integration, maintenance) branches:

- **Branch: `main`**
  - This is the main release branch that contains the latest software code.
  - Lifespan: forever

> ![Branch: main](./images/main-branch.svg)

- **Branches: `release/X.Y.z`**
  - Long-living `release/X.Y.z` branches are _only_ required if there are multiple versions of the software that will be deployed to production, or if there are overlapping releases, or if a maintenance branch is required. The `main` branch should suffice as the _main_ release branch in most simple use-cases.
  - Lifespan: long
  - Naming Conventions:
    - Proper Naming Example: `release/X.Y.z`
      - where `X` and `Y` are positive integers and `z` is the literal 'z'
      - real example: `release/2.3.z`
    - Improper Naming Examples:
      - `release/anything-i-want`
      - `release/2.3.0`
        - violates naming conventions: should end with a 'z', not a digit

> ![Branch: release/X.Y.z](./images/release-XYz-branch.svg)

- **Branches: `hotfix/X.Y.z`**
  - The `hotfix/X.Y.z` branches should only be used in _very_ rare occasions as most hot-fix scenarios can be accomplished in a `release/X.Y.z` branch (also considered a maintenance branch). Hot-fix branches are only necessary when there are multiple releases and all available release branches are occupied with on-going work and cannot perform an adequate or urgent hot-fix.
  - Lifespan: short
  - Naming Conventions:
    - Proper Naming Example: `hotfix/X.Y.z`
      - where `X` and `Y` are positive integers and `z` is the literal 'z'
      - real example: `hotfix/2.3.z`
    - Improper Naming Examples:
      - `hotfix/anything-i-want`
      - `hotfix/2.3.0`
        - violates naming conventions: should end with a 'z', not a digit

> <kbd>NOTE:</kbd> Example list of branches in a project:
>   - `main` _(HEAD = 2.5.0-SNAPSHOT)_
>   - `release/2.3.z` _(HEAD = 2.3.2-SNAPSHOT)_
>   - `release/2.1.z` _(HEAD = 2.1.2-SNAPSHOT)_
>   - `release/1.5.z` _(HEAD = 1.5.3-SNAPSHOT)_
>   - `release/1.1.z` _(HEAD = 1.1.9-SNAPSHOT)_
>   - `release/1.0.z` _(HEAD = 1.0.7-SNAPSHOT)_
>   - ...

> <kbd>NOTE:</kbd> When following these conventions:
>   - the _HEAD_ of every branch is a SNAPSHOT version number
>   - no version number at the _HEAD_ of any _release candidate_ branch can conflict with the _HEAD_ of any other _release candidate_ branch

### Work Branches

These types of branches should be associated to a **Pull Request (PR)** or **Merge Request (MR)** at some point in time, and, consequently, these branches are considered to be _temporary_. These branches are typically merged into an [Integration Branch](#Integration_Branches).

The version specified in the _HEAD_ of this branch should always be a SNAPSHOT version in the build file, in the format `X.Y.Z-SNAPSHOT` where:
- `X`, `Y`, and `Z` are positive integers
- the versioning follows the specification: **[Semantic Versioning](https://semver.org/)**

There are 2 types of _work_ branches:

- **Branches: `feature/*`**
  - This type of branch is where work is done to complete a feature request from an active issue (Example: a feature request where the issue ID is: **ABC-987**). This branch must be attached to a **Pull Request** or **Merge Request** and merged to either `main`, `release/X.Y.z`, or `hotfix/X.Y.z` branch.
  - Lifespan: short
  - Naming Conventions:
    - Proper Naming Example: `feature/ABC-987`
    - Improper Naming Example: `feature/anything-i-want`
- **Branches: `bugfix/*`**
  - This type of branch is where work is done to complete a bug-fix request from an active issue (Example: a bug-fix request where the issue ID is: **DEF-456**). This branch must be attached to a **Pull Request** or **Merge Request** and merged to either `main`, `release/X.Y.z`, or `hotfix/X.Y.z` branch.
  - Lifespan: short
  - Naming Conventions:
    - Proper Naming Example: `bugfix/DEF-456`
    - Improper Naming Example: `bugfix/anything-i-want`

> ![Work Branches](./images/work-branches.svg)

## Release Process

The _main_ release branch should always be the `main` branch. Consequently, main release Git tags should be created on the `main` branch history (commits).

A `release/X.Y.z` branch should not be used as a main release branch. These types of branches are, primarily, created as maintenance branches in order to fix bugs in a release (related to a release that was tagged on the `main` branch), or, secondarily, to work on a _parallel_ (or overlapping) release alongside the `main` branch. If multiple release branches are required then `main` should be used for the _latest_ version of the software, and other versions of the software should go in release branches that follow the naming convention `release/X.Y.z` where `X` and `Y` are positive integers and `z` is the literal 'z'. Example: `release/2.3.z`

When a release is created in a given branch, two commits are necessary in order to appropriately create a release artifact:
1. the first commit strips the `-SNAPSHOT` suffix off of the version number (typically in the build file)
    - version `X.Y.Z-SNAPSHOT` becomes `X.Y.Z`
      - example: `2.3.0-SNAPSHOT` becomes `2.3.0`
    - a tag named `X.Y.Z` is associated to this first commit
      - example tag: `2.3.0`
1. the second commit auto-increments the last digit `Z` (the patch/revision number) in the version number, and re-appends the `-SNAPSHOT` suffix
    - version `X.Y.Z` becomes `X.Y.Z'-SNAPSHOT` (where `Z'` = `Z+1`)
      - example: `2.3.0` becomes `2.3.1-SNAPSHOT`

> <kbd>NOTE:</kbd> Typically, these two commits are produced via an automated release process.

> ![Release Process](./images/release-process.svg)

An example release process would go as follows:
1. all of the relevant **Pull Requests** (or **Merge Requests**) are merged into the `main` branch (the _main_ release branch) where the version is `2.3.0-SNAPSHOT`
1. a release is created in the `main` branch using the release process
    - the first commit strips the `-SNAPSHOT` suffix off of the version number, in this case the version results in: `2.3.0`
      - the tag `2.3.0` is created and points to this first commit (where the source code has the version suffix `-SNAPSHOT` removed)
    - the second commit auto-increments the last digit `Z` (the patch/revision number) in the version number, in this case the new version results in: `2.3.1-SNAPSHOT`
1. the version in the source code of `main` is updated to `2.4.0-SNAPSHOT` manually by a developer in order for new development to continue
1. the `2.3.0` release is tested and a bug is found
1. the release branch `release/2.3.z` (maintenance branch) is created off of the _tag_ `2.3.0` and the version in the source code of this branch is set manually by a developer to: `2.3.1-SNAPSHOT`
1. bug-fix work is done in the `release/2.3.z` branch using the **Pull Request** (or **Merge Request**) process to fix the bug found in the `2.3.0` release, with the testing done on an artifact versioned as `2.3.1-SNAPSHOT`
1. once testing is satisfactory on the `2.3.1-SNAPSHOT` artifact, a release is created in the `release/2.3.z` branch using the release process
    - the first commit strips the `-SNAPSHOT` suffix off of the version number, in this case the version results in: `2.3.1`
      - the tag `2.3.1` is created and points to this first commit (where the source code has the version suffix `-SNAPSHOT` removed)
    - the second commit auto-increments the last digit `Z` (the patch/revision number) in the version number, in this case the new version results in: `2.3.2-SNAPSHOT`
1. subsequent patch/revision releases are performed in the `release/2.3.z` branch with tags `2.3.2`, `2.3.3`, etc. associated to commits living in this branch, and these bug-fixes are merged into `main`, if applicable, on a case-by-case basis, without altering the version number in the source code of `main` which should be at `2.4.0-SNAPSHOT`

> <kbd>NOTE:</kbd> All of the `X.Y.0` tags should be associated to commits that live in the `main` branch, with subsequent patch/revision tags `X.Y.Z'` (where `Z'` is greater than 0) live either on `main` or on their respective `release/X.Y.z` (maintenance) branches.

> ![Continuous Delivery Flow](./images/continuous-delivery-flow.svg)

> <kbd>NOTE:</kbd> If this release process does not work for you then you are not following [Semantic Versioning](https://semver.org/) correctly.

### Hot-fix Scenario

In the unlikely event that an active deployed production release requires an urgent hot-fix then the Git _tag_ for that active production deployment should be found and a Git _branch_ should be created off of that tag following the naming convention `release/X.Y.z` where `X` and `Y` are positive integers and `z` is the literal 'z' (assuming that such a branch does not already exist as there may already be an active and suitable release branch available to perform the hot-fix to do a patch/revision release).

The following example demonstrates the steps for hot-fix-ing a production deployment with version `2.3.1`:
1. the tag representing production deployment is: `2.3.1`
1. locate branch `release/2.3.z` or create branch `release/2.3.z` from the tag `2.3.1`
    - if it is not possible to use branch `release/2.3.z` to perform the hot-fix for whatever reason then create branch: `hotfix/2.3.z`
1. ensure that the software version inside `release/2.3.z` (or `hotfix/2.3.z`) branch is set to the next _logical_ snapshot version
    - example: `2.3.2-SNAPSHOT`
1. perform the hot-fix work in the branch `release/2.3.z` (or `hotfix/2.3.z`) using the **Pull Request** (or **Merge Request**) process
1. the hot-fix artifact will build and publish as version: `2.3.2-SNAPSHOT`
1. once the work is complete and adequately tested, a release is created and tagged in the `release/2.3.z` (or `hotfix/2.3.z`) branch using the release process with the tag `2.3.2`
1. this bug-fix is then merged into `main` branch (if applicable)

> <kbd>NOTE:</kbd> Release candidate (maintenance) branches (`release/X.Y.z`) should be sufficient to perform a hot-fix in the vast majority of hot-fix cases. Only create a `hotfix/X.Y.z` branch if there is an edge-case that conflicts with using a release candidate (maintenance) branch: `release/X.Y.z`

> <kbd>NOTE:</kbd> If this hot-fix process does not work for you then you are not following [Semantic Versioning](https://semver.org/) correctly.

## Diagrams

### DevSecOps Pipeline Architecture

> <img src="./images/continuous-delivery-pipeline-architecture.svg" width="800" alt="Continuous Delivery Pipeline Architecture" title="DevSecOps Pipeline Architecture"/>
